import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static org.junit.Assert.*;

/**
 * Created by v-vlazorenko on 2/2/2016.
 *
 */
//TODO: incomplete
public class JavaAdvanced27IOTest {

    Logger logger = Logger.getLogger(this.getClass().getName());

	public void createFile() {

        String relPathStr = "test"+File.separator+"test.txt";
        String [] parts = relPathStr.split(File.separator);
        ArrayList<String> dirParts = new ArrayList<>();
        for (String part : parts) {
            //par
        }

        Path path = Paths.get("");
        Path dirPath = Paths.get(".");
        path.forEach( p -> {
            if (p != p.getName(path.getNameCount() - 1)) dirPath.resolve(p);
        } );
        System.out.println(dirPath);

        Boolean dirStatus = false;
        dirStatus = dirPath.toFile().mkdirs();

        if (dirStatus) logger.info("Directory created");
        else logger.severe("Directory wasn't created");

        Boolean fileStatus = false;
        if (dirStatus) {
            try {
                fileStatus = path.toFile().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (fileStatus) logger.info("Created file path: "+path.toFile().getCanonicalPath());
                else logger.severe("File wasn't created");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

	}
	
	public void deleteFile() {
        File file = new File("test/test.txt");
        Boolean opStatus = file.delete();
        if (opStatus) logger.info("Directory deleted");
        else logger.severe("Directory wasn't deleted");
	}
	
	
	@Test
	public void testCreateFile() {
		createFile();
		File f = new File("test/test.txt");
		assertTrue(f.exists());
	}
	
	@Test
	public void testDeleteFile() {
		deleteFile();
		File f = new File("test/test.txt");
		assertFalse(f.exists());
		assertFalse(new File("test").exists());
	}

}
