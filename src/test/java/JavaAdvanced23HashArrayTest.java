import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by v-vlazorenko on 1/27/2016.
 *
 */
public class JavaAdvanced23HashArrayTest {

    private static JavaAdvanced23HashArray<String> testHashArray;

    private static JavaAdvanced23HashArray<String> genCanonicalHashArray() {
        testHashArray = new JavaAdvanced23HashArray<>();
        String [] testData = {"String 1", "String 2", "String 3"};
        for (String testElement: testData)
            testHashArray.add(testElement);
        return testHashArray;
    }

    @BeforeClass
    public static void printObjectUnderTest() {
        genCanonicalHashArray().printAll();
    }

    @Before
    public void setUp() throws Exception {
        testHashArray = genCanonicalHashArray();
    }

    @Test
    public void testGetSize() throws Exception {
        assertTrue(testHashArray.getSize()==3);
    }

    @Test
    public void testAdd() throws Exception {
        testHashArray.add("String 4");
        assertTrue(testHashArray.getSize()==4);
        assertTrue(testHashArray.toString().contains("String 4"));
    }

    @Test
    public void testRemove() throws Exception {
        testHashArray.remove("String 1");
        assertTrue(testHashArray.getSize()==2);
        assertFalse(testHashArray.toString().contains("String 1"));
    }

}