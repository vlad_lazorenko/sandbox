import static org.junit.Assert.*;

public class JavaAdvanced22QueueTest {

    private static JavaAdvanced22Queue testQueue = null;

    private static JavaAdvanced22Queue generateTestQueue() {
        JavaAdvanced22Queue generatedQueue = new JavaAdvanced22Queue();
        String[] testValues = {"string 1", "string 2", "string 3"};
        for (String value: testValues) {
            generatedQueue.push(value);
        }
        return generatedQueue;
    }

    @org.junit.BeforeClass
    public static void printTestData() {
        System.out.println("Test data used for each test: "+generateTestQueue());
    }

    @org.junit.Before
    public void init() {
        testQueue = generateTestQueue();
    }

    @org.junit.Test
    public void testPop() throws Exception {
        testQueue.pop();
        assertTrue(testQueue.size()==2);
        assertTrue(testQueue.getHead().getValue().equals("string 2"));
    }

    @org.junit.Test
    public void testPeek() throws Exception {
        testQueue.peek().equals("String 1");
    }

    @org.junit.Test
    public void testPush() throws Exception {
        testQueue.push("string 4");
        assertTrue(testQueue.getLast().getValue().equals("string 4"));
    }

    @org.junit.Test
    public void testSize() throws Exception {
        testQueue.push("string 4");
        testQueue.push("string 4");
        assertTrue(testQueue.size() == 5);
    }

}