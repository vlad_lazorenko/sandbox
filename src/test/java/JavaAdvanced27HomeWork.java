import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by v-vlazorenko on 2/3/2016.
 *
 */
public class JavaAdvanced27HomeWork {

    @SuppressWarnings("")
    List<File> getFiles(File path) {
        List<File> result = new ArrayList<>();
        if (path == null) return result;
        if (path.isDirectory())
            for (File file : Arrays.asList(path.listFiles()))
                result.addAll(getFiles(file));
        else result.add(path);
        return result;
    }

    @Test
    public void getFilesTest() {
        getFiles(new File(".")).forEach(
            f -> {
                try {
                    System.out.println(f.getCanonicalFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        );
    }

    public long calculateFiles() {
        return Stream.of(getFiles(new File("."))).count();
    }

}
