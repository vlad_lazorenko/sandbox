
class HelloRubyClass
  def initialize(name = "Clazz")
    @name = name
    puts name
  end

  def hi
    puts "Hi class"
  end

  def hiParam(param)
    puts "Hi #{param}"
  end
end

@instance = HelloRubyClass.new("passed")
instance.hi
instance.hiParam("passedParam")

