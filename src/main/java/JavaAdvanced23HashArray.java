import java.util.ArrayList;

/**
 * Created by v-vlazorenko on 1/27/2016.
 *
 */
public class JavaAdvanced23HashArray<T> {

    private int size;
    private int buckets;

    private ArrayList<T> [] pool;

    public JavaAdvanced23HashArray(int buckets) {
        this.buckets = buckets;
        pool = new ArrayList[buckets];
        for (int i = 0; i < buckets; i++) {
            pool[i] = new ArrayList<>();
        }
    }

    public JavaAdvanced23HashArray() {
        this(10);
    }

    public int getSize() {
        return size;
    }

    private int getIndex(T obj) {
        return Math.abs(obj.hashCode()) % this.buckets;
    }

    public void add(T obj) {
        pool[getIndex(obj)].add(obj);
        size++;
    }

    public void remove(T obj) {
        pool[getIndex(obj)].remove(obj);
        size--;
    }

    public void printAll() {
        System.out.println(toString());
    }

    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append('[');
        int lastNonEmptyIndex = 0;
        for (int j = 0; j<this.buckets; j++) {
            if (!pool[j].isEmpty())
                lastNonEmptyIndex = j;
        }

        for (int j = 0; j<this.buckets; j++) {
            ArrayList<T> swimmer = pool[j];
            if (swimmer!=null) {
                for (T element : swimmer) {
                    sb.append(element);
                    if (element!=swimmer.get(swimmer.size() - 1))
                        sb.append(',');
                }
            }

            if (buckets-1 != j && !swimmer.isEmpty() && j!=lastNonEmptyIndex)
                sb.append(',');
        }
        sb.append(']');

        return sb.toString();
    }

}
