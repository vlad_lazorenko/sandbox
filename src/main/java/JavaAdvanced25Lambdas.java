import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Created by v-vlazorenko on 1/29/2016.
 *
 */
public class JavaAdvanced25Lambdas {

    Function<String, Integer> function = String::length;
    BiFunction<String, String, Integer> length = (s1,s2) -> s1.length()+s2.length();

}
