import com.sun.org.apache.xpath.internal.SourceTree;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Logger;

/**
 * Created by v-vlazorenko on 1/28/2016.
 * Написать метод:
 * 1) Принимает Class, возращает экземпляр данного класса
 * 2) принимает Объект, выводит на экран все приватные методы
 * 3) принимает Объект, выводит на экран все методы которые возвращают строку
 * 4) принимает Объект, выводит все поля, имена которых длинее 4х символов
 * 5) Принимает объект и имя метода. Вызывает этот метод на данном объекте
 * 6) Принимает объект, возвращает коллекцию классов всех предков
 * 7) Принимает класс, вывод на экран имена всех final методов, отсортированых в обратном порядке
 */
public class JavaAdvanced24Reflection {

    public static void main(String[] args) throws Exception {
        Class clazz = Person2.class;

        Person2 person = instantiateClazz(clazz);
        printPrivateMethods(person);

    }

    private static void printPrivateMethods(Person2 person) {
        System.out.print("1) Принимает Class, возращает экземпляр данного класса: ");
        Class clazz = person.getClass();

        Arrays.stream(clazz.getDeclaredMethods())
                .filter(m -> Modifier.isPrivate(m.getModifiers()))
                .map(x -> "["+x+"] ")
                .forEach(System.out::print);
    }

    private static Person2 instantiateClazz(Class clazz) throws InstantiationException, IllegalAccessException {
        return (Person2) clazz.newInstance();
    }

}

class Person2 {
    private String name;
    private int age;

    public double calculateSalary() {
        return salaryLogic() * Math.random();
    }

    private double salaryLogic() {
        return age * name.length();
    }
}

class ToliaReflectionDemo {

    public static void main(String... args) throws Exception {
        String[] array = new String[5];
        for (int i = 0; i < array.length; i++) {
            array[i] = "Tolik";
        }

        String name = "Tolik";
        Class clazz = name.getClass();
        Field field = clazz.getDeclaredField("value");
        Method[] declaredMethods = clazz.getMethods();
        for (Method m : declaredMethods) {
            System.out.println(m);
        }

        field.setAccessible(true);
        //char[] chars = (char[]) field.get(name);
        char[] newName = {'K', 'o', 'l', 'y', 'a'};
        field.set(name, newName);
        field.setAccessible(false);
        //System.out.println(field);

        for (int i = 0; i < array.length; i++) {
          System.out.println(array[i]);
        }
    }
}