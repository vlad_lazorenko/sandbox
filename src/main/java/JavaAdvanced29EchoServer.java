import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by v-vlazorenko on 2/4/2016.
 *
 */
public class JavaAdvanced29EchoServer {

    public static void main(String ... args) throws Exception {
        startEchoServer();
//        startGreetingsServer();
    }

    private static void startEchoServer() throws IOException {
        ServerSocket serverSocket = new ServerSocket(50000);
        Socket socket = serverSocket.accept();
        OutputStream outputStream = socket.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
        writer.write("Hey\r\n");
        writer.flush();
        while(true) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String in = reader.readLine();
            if (in.equals("exit")) { reader.close(); writer.close(); break; }
            writer.write(in+"\r\n");
            writer.flush();
        }
    }

    private static void startGreetingsServer() throws IOException, InterruptedException {
        ServerSocket serverSocket = new ServerSocket(50000);
        while (true) {
            Socket socket = serverSocket.accept();
            OutputStream outputStream = socket.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            writer.write("Hey");
            writer.flush();
            Thread.sleep(5000);
            writer.close();
        }
    }

}
