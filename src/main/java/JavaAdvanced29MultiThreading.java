import java.util.List;

/**
 * Created by v-vlazorenko on 2/4/2016.
 *
 */
public class JavaAdvanced29MultiThreading {

    public static void main(String ... args) throws InterruptedException {
        System.out.println(Thread.currentThread());
//Bad style usage
//        MyThread thread1 = new MyThread();
//        MyThread thread2 = new MyThread();
        Thread thread1 = new Thread(new Task());
        Thread thread2 = new Thread(new Task());
        thread1.start();
        Thread.sleep(500);
        thread2.start();
    }

}

//Bad style, because of inheritance: class MyThread extends Thread {
class Task implements Runnable {

    public void run() {
        for (int i=0; i<5; i++) {
            System.out.print(Thread.currentThread());
            System.out.println(" i = " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
