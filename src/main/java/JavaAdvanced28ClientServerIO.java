import java.io.*;
import java.util.Date;

/**
 * Created by v-vlazorenko on 2/3/2016.
 *
 */
public class JavaAdvanced28ClientServerIO {

    public static void main(String ... args) {

        String fileName = "message.txt";

        String input = readInput();

        saveMessageObject(fileName, input);

        Message message = readMessageObject(fileName);

        System.out.println(message);

    }

    private static Message readMessageObject(String fileName) {
        Message message = null;
        try {
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            message = (Message) ois.readObject();
            ois.close();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return message;
    }

    private static void saveMessageObject(String fileName, String messageToSave) {
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            Message message = new Message(new Date(), messageToSave);
            oos.writeObject(message);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readInput() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = null;
        while(input==null) {
            try {
                input = br.readLine();
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return input;
    }

}

class Message implements Serializable {

    private static long serialVersionUID = 348764382765L;

    private String message = null;
    private Date date = null;

    public Message(Date date, String message) {
        this.date = date;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Message{" +
                "date=" + date + '\'' +
                ", message='" + message +
                '}';
    }
}