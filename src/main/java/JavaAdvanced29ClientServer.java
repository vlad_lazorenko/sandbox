import java.io.*;
import java.util.Date;

/**
 * Created by v-vlazorenko on 2/4/2016.
 *
 */
public class JavaAdvanced29ClientServer {

    public static void main(String ... args) throws Exception {
        String fileName = "binaryMessage.bin";
        String messageText = JavaAdvanced28ClientServerIO.readInput();

        BinaryMessage bmessage = new BinaryMessage(1,new Date(),messageText);
        FileOutputStream fos = new FileOutputStream(fileName);
        DataOutputStream dos = new DataOutputStream(fos);
        dos.writeInt(bmessage.getId());
        dos.writeLong(bmessage.getDate().getTime());
        dos.writeShort(bmessage.getMessage().length());
        dos.writeChars(bmessage.getMessage());

        FileInputStream fis = new FileInputStream(fileName);
        DataInputStream dis = new DataInputStream(fis);
        int id = dis.readInt();
        Date date = new Date(dis.readLong());
        short messageLength = dis.readShort();
        StringBuilder messageTextBuilder = new StringBuilder();
        for(int i=0; i<messageLength; i++)
            messageTextBuilder.append(dis.readChar());
        BinaryMessage readMessage = new BinaryMessage(id,date,messageTextBuilder.toString());

        System.out.println(readMessage);
    }

}

class BinaryMessage {

    private int id;
    private Date date;
    private String message;

    public BinaryMessage(int id, Date date, String message) {
        this.id = id;
        this.date = date;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "BinaryMessage{" +
                "id=" + id +
                ", date=" + date +
                ", message='" + message + '\'' +
                '}';
    }

}

