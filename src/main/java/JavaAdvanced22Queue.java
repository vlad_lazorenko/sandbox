public class JavaAdvanced22Queue {

    private Node head = null;
    private int size = 0;

    public Object pop() {
        Node curHead = head;
        head = curHead.next;
        --size;
        return curHead.getValue();
    }

    public Object peek() {
        return head.getValue();
    }

    public void push(Object obj) {

        if (size == 0) {
            head = new Node(null, obj);
        } else {
            Node last = getLast();
            last.next = new Node(null, obj);
        }

        ++size;

    }

    public int size() {
       return size;
    }

    public Node getLast() {
        Node search = head;
        while (search != null && search.hasNext()) {
            search = search.getNext();
        }
        return search;
    }

    public Node getHead() {
        return head;
    }

    public String toString() {
        Node cur = this.getHead();
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        while (cur.hasNext()) {
            sb.append(cur.getValue()+",");
            cur = cur.getNext();
        }
        sb.append(cur.getValue()+"]");

        return sb.toString();
    }

}

class Node {

    protected Node next;
    private Object value;

    public Node(Node next, Object value) {
        this.next = next;
        this.value = value;
    }

    public boolean hasNext() {
        return (this.next != null);
    }

    public Node getNext() {
        return next;
    }

    public Object getValue() {
        return value;
    }

}

