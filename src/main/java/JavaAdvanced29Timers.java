import java.util.*;

/**
 * Created by v-vlazorenko on 2/4/2016.
 *
 */
public class JavaAdvanced29Timers implements Runnable {

    public static void main(String ... args) throws InterruptedException {
        Thread t1 = new Thread(new JavaAdvanced29Timers(10));
        Thread t2 = new Thread(new JavaAdvanced29Timers(15));
        t1.start();
        t2.start();
    }

    private int seconds;

    public JavaAdvanced29Timers(int seconds) {
        this.seconds = seconds;
    }

    @Override
    public void run() {
        int curSec = 0;
        while (curSec<seconds) {
            try {
                Thread.sleep(1000);
                curSec++;
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(""+Thread.currentThread() + (seconds - curSec) + " seconds left");
        }
    }
}

class JavaAdvanced29Maximizer implements Runnable {

    public static void main(String ... args) throws InterruptedException {
        Thread t1 = new Thread(new JavaAdvanced29Maximizer(Arrays.asList(1,2,42,3)));
        Thread t2 = new Thread(new JavaAdvanced29Maximizer(Arrays.asList(434,8765,38)));
        t1.start();
        t2.start();
    }

    private List<Integer> list = null;

    public JavaAdvanced29Maximizer(List<Integer> list) {
        this.list = list;
    }

    @Override
    public void run() {
        System.out.println(
            "" + Thread.currentThread() + " max value " + list.parallelStream().max((o1, o2) -> o1-o2));
    }

}

class JavaAdvanced29GenericMaximizer<E> implements Runnable {

    public static void main(String ... args) throws InterruptedException {
        Thread t1 =
            new Thread(
                new JavaAdvanced29GenericMaximizer<>(
                    Arrays.asList("Str4", "String7", "s2"),
                    (o1,o2) -> o1.length() - o2.length()
                )
            );
        Thread t2 =
            new Thread(
                new JavaAdvanced29GenericMaximizer<>(
                    Arrays.asList(
                        new Message(new Date(),"m1"),
                        new Message(new Date(new Date().getTime()-10000),"")
                    ),
                    (o1,o2) -> (int)(o1.getDate().getTime()-o2.getDate().getTime()
                    )
                )
            );
        t1.start();
        t2.start();
    }

    private List<E> list = null;
    private Comparator<E> comparator = null;

    public JavaAdvanced29GenericMaximizer(List<E> list, Comparator<E> comparator) {
        this.list = list;
        this.comparator = comparator;
    }

    @Override
    public void run() {
        System.out.println(
            "" + Thread.currentThread() + " max value " + list.parallelStream().max(comparator));
    }

}