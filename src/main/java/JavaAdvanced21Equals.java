//public class JavaAdvanced2 {
//
//    public static void main(String [] args) {
//        System.out.println();
//    }
//
//}
//
//class Person {
//    int id;
//    String name;
//
//    public Person(int id, String name) {
//        this.id = id;
//        this.name = name;
//    }
//
//    public boolean equals(Object object) {
//        if (this == object) {
//            return true;
//        }
//        if (object instanceof Person) {
//            Person person = (Person) object;
//            if (id != person.id) {
//                return false;
//            }
//            if (name != null) {
//                if (!name.equals(person.name)) {
//                    return false;
//                }
//            } else {
//                if (person.name != null) {
//                    return false;
//                }
//            }
//            return true;
//        }
//
//        return false;
//    }
//}

public class JavaAdvanced21Equals {
    public static void main(String[] args) {
        Person person1 = new Person(true, "text", new Phone(1, "123"));
        Person person2 = new Person(true, "text", new Phone(1, "123"));
        System.out.println(person1.equals(person2)); // true
        System.out.println(person1.equals(null)); // false
    }


}

class Person {
    boolean alive;
    String name;
    Phone phone;

    public Person(boolean alive, String name, Phone phone)  {
        this.alive = alive;
        this.name = name;
        this.phone = phone;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object instanceof Person) {

            Person person = (Person) object;

            if (name != null) {
                if (!name.equals(person.name)) {
                    return false;
                }
            } else {
                if (person.name != null) {
                    return false;
                }
            }

            if (phone != null) {
                if (!phone.equals(person.phone)) {
                    return false;
                }
            } else {
                if (person.phone != null) {
                    return false;
                }
            }

            if (alive == person.alive) return true;
        }

        return false;
    }
}

class Phone {
    int code;
    String number;

    public Phone(int code, String number) {
        this.code = code;
        this.number = number;

    }

    public boolean equals(Object object) {

        if (this == object) {
            return true;
        }

        if (object instanceof Phone) {

            Phone phone = (Phone) object;

            if (code != phone.code) {
                return false;
            }
            if (number != null) {
                if (!number.equals(phone.number)) {
                    return false;
                }
            } else {
                if (phone.number != null) {
                    return false;
                }
            }

            return true;
        }

        return false;

    }
}