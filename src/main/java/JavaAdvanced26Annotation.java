import scala.annotation.meta.field;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

/**
 * Created by v-vlazorenko on 2/1/2016.
 *
 */
public class JavaAdvanced26Annotation {

    public static void main(String[] args) {
        Person26 person = new Person26();
        person.name = "John";
        System.out.println(person);
        injectAnnotation(person);
        System.out.println(person);
    }

    private static void injectAnnotation(Person26 person) {

        Class clazz = person.getClass();
        Field[] declaredFields = clazz.getDeclaredFields();
        Stream.of(declaredFields)
            //Anonymous object implementation.
            // Oracle doesn't recommend to use forEach, due to possible unsafe data modification.
            .map( field -> new Object() {
                    Field fieldForAssigment = field;
                    RandomInt annotation = field.getDeclaredAnnotation(RandomInt.class);
                }
            )
            .filter( obj -> obj.annotation!=null )
            .forEach( x -> {
                try {
                    x.fieldForAssigment.setInt(person, x.annotation.min()+new Random().nextInt(x.annotation.max()));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } );
//            .forEach( field -> {
//                RandomInt annotation = field.getDeclaredAnnotation(RandomInt.class);
//                if (annotation!=null) {
//                    try {
//                        field.setInt(person, annotation.min()+new Random().nextInt(annotation.max()));
//                    } catch (IllegalAccessException e) {
//                        e.printStackTrace();
//                    }
//                }
//            } );
    }

}

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@interface RandomInt {
    int min();
    int max();
}

class Person26 {

    String name;

    @RandomInt(min = 7, max = 18)
    int age;

    @Override
    public String toString() {
        return "Person {" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

}