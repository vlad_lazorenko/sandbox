import com.sun.javafx.geom.transform.Identity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Created by v-vlazorenko on 2/1/2016.
 *
 */
public class JavaAdvanced26Streams {

    int currentTime = 0;

    public static void main(String ... args) {
        List<Integer> list = new ArrayList<Integer>() {
            {
                add(3);
                add(4);
                add(24);
                add(4647);
            }
        };

        parStreamIntSummator(list);
//        System.out.println("Non-parallel: "+currentTime);
    }

    public static int parStreamIntSummator(List<Integer> list) {
        return list.stream().reduce(0, (x, y) -> x + y);
    }

    /*public static <M,P,R> void executeTimedMethod(M method, ) {

    }

    public static void runTimed(Function<List<Integer>, Integer> func) {
        //Function<List<Integer>, Integer> listSum = JavaAdvanced26Streams::parStreamIntSummator;
        func
    } */

}

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface Timed {}