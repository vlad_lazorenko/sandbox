package server;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Teacher_vas on 2/4/2016.
 */
public class Message implements Serializable {
    String nick;
    String message;
    Date date;

    public Message(String nick, String message, Date date) {
        this.nick = nick;
        this.message = message;
        this.date = date;
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        return nick + " " + simpleDateFormat.format(date) + " -> " +
                message;
    }
}
