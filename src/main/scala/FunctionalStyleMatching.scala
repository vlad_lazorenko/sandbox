val p1: PartialFunction[Any, Int] = { case _: Int => 1 }
println(p1.getClass)
val p2 = { case "Java" => 0 }
val p3 = { case "Rust" => 50 }
val processing = p1.orElse(p2).orElse(p3)
val result = processing.applyOrElse("Rusty", {_:String => 33})