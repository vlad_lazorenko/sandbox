import java.util

class SimpleMap {
  private[this] var m = new util.HashMap[String, String]()
  def size: Int = m.size()
  def filter(p: ((String,String)) => Boolean): SimpleMap = {
    val m1 = new util.HashMap[String, String]()
    val iter = m.entrySet().iterator()
    while (iter.hasNext) {
      val pair = iter.next()
      val tuple = (pair.getKey, pair.getValue)
      if (p(tuple)) m.put(pair.getKey, pair.getValue)
    }
    this.m = m1
    this
  }
  def filterKey(p: String => Boolean): SimpleMap = ???

}