class Map {
  def apply(k: Int) = k.toString
}

object Tmp {
  val m = new Map()
  m(2) == "2"


  val pf: PartialFunction[Any, String] = {
    case s: String if s == "check" => "text"
  }
}